# kata-node-can-i-go

## Author

- Victoria Doe

## Software

- Node v16.15.1 - Express v4.18.2
- Typescript v4.9.5
- MongoDB
- Docker

## Subject

An application that manages Passes that deliver access to Users to private zones

## How to run the project

To run the project locally, first make sure you have Node and npm installed.  
Clone this [URL]("git@gitlab.com:victoria-cs-doe/kata-node-can-i-go.git")

You must first install all the node_modules by executing the following command :
```bash
  npm install
```

To run the project in development mode (watch mode i.e., each change to a file will restart the server and be instantly taken into account) :

```bash
  npm run dev
```

To run the project in production mode

```bash
  npm run start
```

To launch the database and server, open a terminal at the root of the project (outside the src folder) :

```bash
  docker-compose build  
  docker-compose up
```

Retrieve container ids :

```bash
  docker ps
```

To shut the database and the server down :

```bash
  docker stop container-id
```

You will also need to set the following environment variables in a .env file at the root of the project (do not wrap variables in quotes):

| Environment Variable | Default Value | Type    |
| -------------------- | ------------- | ------- |
| SERVER_PORT          | 3005          | integer |
| MONGO_DB_URL         | hidden        | string  |
| SECRET               | hidden        | string  |


This .env file is mandatory for the docker-compose.yml to start the database and the server


## Queries
1. Retrieve all passes
2. Retrieve a pass by id
3. Create a pass
4. Update a pass
5. Delete a pass
6. Retrieve all places
7. Retrieve a place by id
8. Create a place
9. Update a place
10. Delete a place
11. Retrieve all users
12. Retrieve a user by id
13. Create a user
14. Update a user
15. Delete a user
16. Log a user in
17. Retrieve all accessible places by a user
18. Determine if place is accessible to user
