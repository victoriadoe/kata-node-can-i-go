/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
    clearMocks: true,
    preset: 'ts-jest',
    testEnvironment: 'node',
    moduleFileExtensions: ['js', 'json', 'ts'],
    coverageDirectory: '__tests__/coverage',
    roots: ['<rootDir>'],
    testMatch: ['**/?(*.)+(spec).ts'],
    transform: {
        '^.+\\.(t|j)s$': 'ts-jest'
    },
    testTimeout: 20000,
    globalSetup: './__tests__/global-setup.ts'
}
