FROM node:20.9.0-bullseye as build

WORKDIR /build

COPY package*.json /build

RUN npm install

ADD . /build

RUN npm run build

FROM node:20.9.0-bullseye

WORKDIR /app

COPY package*.json /app

RUN npm install --omit=dev

COPY --from=build /build/dist/ /app

EXPOSE 3800

CMD ["node", "/app/server.js"]
