import joi from 'joi'
import { CreateUserDto } from '../../user/dto/create-user-dto'
import { CreateOrUpdatePassDto } from '../../pass/dto/create-or-update-pass-dto'
import { CreateOrUpdatePlaceDto } from '../../place/dto/create-or-update-place-dto'
import { LoginDto } from '../../auth/dto/login-dto'

const options = {
    abortEarly: true,
    allowUnknown: true,
    stripUnknown: true
}

export const validateUser = (user: unknown) => {
    const schema = joi.object<CreateUserDto>({
        pass_id: joi.string().alphanum().required(),
        first_name: joi.string().alphanum().required(),
        last_name: joi.string().alphanum().required(),
        age: joi.number().integer().required(),
        phone_number: joi.string().required(),
        address: joi.string().required(),
        email: joi.string().email().required(),
        password: joi.string().required()
    })
    return schema.validate(user, options)
}

export const validatePass = (pass: unknown) => {
    const schema = joi.object<CreateOrUpdatePassDto>({
        level: joi.number().integer().min(1).max(5).required(),
        created_at: joi.date().default(new Date()),
        updated_at: joi.date().default(new Date())
    })
    return schema.validate(pass, options)
}

export const validatePlace = (place: unknown) => {
    const schema = joi.object<CreateOrUpdatePlaceDto>({
        address: joi.string().required(),
        phone_number: joi.string().required(),
        required_pass_level: joi.number().integer().min(1).max(5).required(),
        required_age_level: joi.number().integer().required()
    })
    return schema.validate(place, options)
}

export const validateLogin = (login: unknown) => {
    const schema = joi.object<LoginDto>({
        email: joi.string().email().required(),
        password: joi.string().required()
    })
    return schema.validate(login, options)
}
