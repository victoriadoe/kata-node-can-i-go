import { UnsupportedMediaType } from '../../middleware/exception/Exception'
import { Request, Response, NextFunction } from 'express'

export const jsonHeaders = (req: Request, res: Response, next: NextFunction) => {
    res.set('Content-Type', 'application/json')
    res.set('Accept', 'application/json')
    next()
}

const requireJSON = (req: Request) => {
    const content = req.headers['content-type']
    return content === 'application/json' || content === '*/*'
}

export const validateRequest = (req: Request, res: Response, next: NextFunction) => {
    if (!requireJSON(req)) {
        return next(new UnsupportedMediaType('Please make sure the request content type is json'))
    }
    return next()
}
