import { Router } from 'express'
import { autoCatch } from '../../middleware/auto-catch'
import PlaceController from '../controller/place-controller'
import { validateRequest } from '../../helpers/validators/request-validator'
import { requireAuthentication } from '../../middleware/auth'

const placeController: PlaceController = new PlaceController()
const router: Router = Router()

router.get('/', autoCatch(placeController.findAllPlaces))
router.get('/:id', autoCatch(placeController.findPlaceById))
router.post('/', validateRequest, requireAuthentication, autoCatch(placeController.createPlace))
router.patch('/:id', validateRequest, requireAuthentication, autoCatch(placeController.updatePlace))
router.delete('/:id', requireAuthentication, autoCatch(placeController.deletePlace))

export default router
