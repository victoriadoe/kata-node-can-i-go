import { Schema, model } from 'mongoose'

export interface IPlace {
    _id?: string
    address: string
    phone_number: string
    required_pass_level: number
    required_age_level: number
}

const PlaceSchema = new Schema<IPlace>({
    address: {
        type: String,
        required: true
    },
    phone_number: {
        type: String,
        required: true
    },
    required_pass_level: {
        type: Number,
        required: true,
        min: 1,
        max: 5
    },
    required_age_level: {
        type: Number,
        required: true
    }
})

export const Place = model('Place', PlaceSchema)
