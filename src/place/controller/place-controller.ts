import { NextFunction, Request, Response } from 'express'
import PlaceService from '../service/place-service'
import { BadRequestError, NotFoundError } from '../../middleware/exception/Exception'
import { validatePlace } from '../../helpers/validators/payload-validator'

export default class PlaceController {
    private placeService: PlaceService = new PlaceService()

    findAllPlaces = async (req: Request, res: Response) => {
        const places = await this.placeService.findAll()
        res.status(200).json({ places: places, message: 'Places successfully retrieved' })
    }

    findPlaceById = async (req: Request, res: Response, next: NextFunction) => {
        const place = await this.placeService.findById(req.params.id)
        if (!place) {
            throw next(new NotFoundError(`Place with id ${req.params.id} does not exist`))
        }
        res.status(200).json({ place: place, message: 'Place successfully retrieved' })
    }

    createPlace = async (req: Request, res: Response, next: NextFunction) => {
        const placeDto = validatePlace(req.body)
        if (placeDto.error) {
            return next(new BadRequestError(placeDto.error.details[0].message))
        }
        const place = await this.placeService.create(placeDto.value)
        res.status(201).json({ place: place, message: 'Place successfully created' })
    }

    updatePlace = async (req: Request, res: Response, next: NextFunction) => {
        const placeDto = validatePlace(req.body)
        if (placeDto.error) {
            return next(new BadRequestError(placeDto.error.details[0].message))
        }
        const place = await this.placeService.update(req.params.id, placeDto.value)
        if (!place) {
            throw next(new NotFoundError(`Place with id ${req.params.id} does not exist`))
        }
        res.status(200).json({ place: place, message: 'Place successfully updated' })
    }

    deletePlace = async (req: Request, res: Response, next: NextFunction) => {
        const deletedPlace = await this.placeService.delete(req.params.id)
        if (!deletedPlace) {
            throw next(new NotFoundError(`Place with id ${req.params.id} does not exist`))
        }
        res.status(200).json({ message: 'Place successfully deleted' })
    }
}
