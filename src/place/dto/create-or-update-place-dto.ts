import { IPlace } from '../schema/Place'

export type CreateOrUpdatePlaceDto = Omit<IPlace, '_id'>
