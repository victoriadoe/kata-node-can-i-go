import { IPlace, Place } from '../schema/Place'
import { CreateOrUpdatePlaceDto } from '../dto/create-or-update-place-dto'

export default class PlaceService {
    findAll = async (): Promise<IPlace[]> => {
        return Place.find()
    }

    findById = async (id: string): Promise<IPlace | null> => {
        return Place.findById(id)
    }

    findAllHavingPassAndAgeLevel = async (passLevel: number, ageLevel: number): Promise<IPlace[]> => {
        return Place.find({ required_pass_level: { $lte: passLevel }, required_age_level: { $lte: ageLevel } })
    }

    create = async (createPlace: CreateOrUpdatePlaceDto): Promise<CreateOrUpdatePlaceDto> => {
        return Place.create(createPlace)
    }

    update = async (id: string, updatePlace: CreateOrUpdatePlaceDto): Promise<CreateOrUpdatePlaceDto | null> => {
        return Place.findByIdAndUpdate(id, updatePlace, { new: true })
    }

    delete = async (id: string) => {
        return Place.findByIdAndDelete(id)
    }
}
