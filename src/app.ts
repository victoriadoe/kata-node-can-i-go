import express from 'express'
import routes from './routes'
import helmet from 'helmet'
import errorHandler from './middleware/error-handler'
import { jsonHeaders } from './helpers/validators/request-validator'
import swaggerUI from 'swagger-ui-express'
import { swaggerDocumentation } from './config/documentation/swagger-documentation'

const app = express()

//Documentation
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocumentation))

//Helpers
app.use(jsonHeaders)
app.use(express.json())
app.use(helmet())

//Routes
app.use('/', routes)

//Middleware
app.use(errorHandler)

export default app
