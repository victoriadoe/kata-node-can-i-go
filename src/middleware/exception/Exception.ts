export class BaseError {
    message: string
    status: number

    constructor(message: string, status = 500) {
        this.message = message
        this.status = status
    }
}

export class BadRequestError extends BaseError {
    constructor(message: string) {
        super(message, 400)
    }
}

export class AuthenticationFailureError extends BaseError {
    constructor(message: string) {
        super(message, 401)
    }
}

export class UnauthorizedError extends BaseError {
    constructor(message: string) {
        super(message, 403)
    }
}

export class NotFoundError extends BaseError {
    constructor(message: string) {
        super(message, 404)
    }
}

export class ResourceConflictError extends BaseError {
    constructor(message: string) {
        super(message, 409)
    }
}

export class UnsupportedMediaType extends BaseError {
    constructor(message: string) {
        super(message, 415)
    }
}

export class DatabaseError extends BaseError {
    constructor() {
        super('Database error, please check your JSON body for invalid data', 500)
    }
}
