import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken'
import { SECRET } from '../config/lib/dotenv'
import { AuthenticationFailureError } from './exception/Exception'

export const getDecodedPayload = (req: Request, res: Response, next: NextFunction) => {
    const authorization = req.headers.authorization
    const token = authorization?.split(' ')[1]
    if (token) {
        try {
            return jwt.verify(token, SECRET)
        } catch (err) {
            return next(new AuthenticationFailureError('Invalid or expired token'))
        }
    }
}

export const requireAuthentication = (req: Request, res: Response, next: NextFunction) => {
    const getToken = getDecodedPayload(req, res, next)
    if (getToken) {
        return next()
    }
    return next(new AuthenticationFailureError('Missing auth token'))
}
