import { CreateUserDto } from '../dto/create-user-dto'
import { IUser, User } from '../schema/User'
import { UpdateUserDto } from '../dto/update-user-dto'
import bcrypt from 'bcrypt'

export default class UserService {
    findAll = async (): Promise<IUser[]> => {
        return User.find()
    }

    findById = async (id: string | undefined): Promise<IUser | null> => {
        return User.findById(id)
    }

    findByEmail = async (email: string): Promise<IUser | null> => {
        return User.findOne({ email: email })
    }

    create = async (createUser: CreateUserDto): Promise<CreateUserDto> => {
        return User.create(createUser)
    }

    update = async (id: string, updateUser: UpdateUserDto): Promise<UpdateUserDto | null> => {
        return User.findByIdAndUpdate(id, updateUser, { new: true })
    }

    delete = async (id: string) => {
        return User.findByIdAndDelete(id)
    }

    hashPassword = async (password: string) => {
        return await bcrypt.hash(password, 10)
    }

    verifyPassword = async (plain: string, hash: string) => {
        return await bcrypt.compare(plain, hash)
    }
}
