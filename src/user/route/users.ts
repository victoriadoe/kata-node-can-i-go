import { Router } from 'express'
import { autoCatch } from '../../middleware/auto-catch'
import UserController from '../controller/user-controller'
import { validateRequest } from '../../helpers/validators/request-validator'
import { requireAuthentication } from '../../middleware/auth'

const userController: UserController = new UserController()
const router: Router = Router()

router.get('/', autoCatch(userController.findAllUsers))
router.get('/:id', autoCatch(userController.findUserById))
router.get('/:user_id/:place_id', autoCatch(userController.hasAccessToPlace))
router.post('/', validateRequest, autoCatch(userController.createUser))
router.post('/:user_id/places', requireAuthentication, autoCatch(userController.retrieveAccessiblePlaces))
router.patch('/:id', requireAuthentication, validateRequest, autoCatch(userController.updateUser))
router.delete('/:id', requireAuthentication, autoCatch(userController.deleteUser))

export default router
