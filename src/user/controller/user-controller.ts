import { NextFunction, Request, Response } from 'express'
import { validateUser } from '../../helpers/validators/payload-validator'
import UserService from '../service/user-service'
import { BadRequestError, NotFoundError, UnauthorizedError } from '../../middleware/exception/Exception'
import PassService from '../../pass/service/pass-service'
import PlaceService from '../../place/service/place-service'

export default class UserController {
    private userService: UserService = new UserService()
    private passService: PassService = new PassService()
    private placeService: PlaceService = new PlaceService()

    findAllUsers = async (req: Request, res: Response) => {
        const users = await this.userService.findAll()
        res.status(200).json({ users: users, message: 'Users successfully retrieved' })
    }

    findUserById = async (req: Request, res: Response, next: NextFunction) => {
        const user = await this.userService.findById(req.params.id)
        if (!user) {
            throw next(new NotFoundError(`User with id ${req.params.id} does not exist`))
        }
        res.status(200).json({ user: user, message: 'User successfully retrieved' })
    }

    createUser = async (req: Request, res: Response, next: NextFunction) => {
        const userDto = validateUser(req.body)
        if (userDto.error) {
            return next(new BadRequestError(userDto.error.details[0].message))
        }
        userDto.value.password = await this.userService.hashPassword(userDto.value.password)
        const user = await this.userService.create(userDto.value)
        const { password, ...result } = user
        res.status(201).json({ user: result, message: 'User successfully created' })
    }

    updateUser = async (req: Request, res: Response, next: NextFunction) => {
        const user = await this.userService.update(req.params.id, req.body)
        if (!user) {
            throw next(new NotFoundError(`User with id ${req.params.id} does not exist`))
        }
        res.status(200).json({ user: user, message: 'User successfully updated' })
    }

    deleteUser = async (req: Request, res: Response, next: NextFunction) => {
        const deletedUser = await this.userService.delete(req.params.id)
        if (!deletedUser) {
            throw next(new NotFoundError(`User with id ${req.params.id} does not exist`))
        }
        res.status(200).json({ message: 'User successfully deleted' })
    }

    hasAccessToPlace = async (req: Request, res: Response, next: NextFunction) => {
        const user = await this.userService.findById(req.params.user_id)
        if (!user) {
            throw next(new NotFoundError(`User with id ${req.params.user_id} does not exist`))
        }
        const pass = await this.passService.findById(String(user.pass_id))
        const place = await this.placeService.findById(req.params.place_id)

        if (!place) {
            throw next(new NotFoundError(`Place with id ${req.params.place_id} does not exist`))
        }
        if (pass && pass.level < place.required_pass_level) {
            return next(new UnauthorizedError(`You don't have the sufficient pass level to access this place`))
        }
        if (user.age < place.required_age_level) {
            return next(new UnauthorizedError(`You aren't old enough to access this place`))
        }
        res.status(200).json({ message: 'You can access this place' })
    }

    retrieveAccessiblePlaces = async (req: Request, res: Response, next: NextFunction) => {
        const user = await this.userService.findById(req.params.user_id)
        if (!user) {
            throw next(new NotFoundError(`User with id ${req.params.id} does not exist`))
        }
        const pass = await this.passService.findById(user.pass_id.toString())
        const accessiblePlaces = pass && (await this.placeService.findAllHavingPassAndAgeLevel(pass.level, user.age))
        if (!accessiblePlaces?.length) {
            return next(new UnauthorizedError(`You don't have access to any place`))
        }
        res.status(200).json({ message: 'You can access the following places : ', places: accessiblePlaces })
    }
}
