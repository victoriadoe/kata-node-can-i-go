import { IUser } from '../schema/User'

export type CreateUserDto = Omit<IUser, '_id'>
