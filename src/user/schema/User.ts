import { Schema, model } from 'mongoose'

export interface IUser {
    _id?: string
    pass_id: string
    first_name: string
    last_name: string
    age: number
    phone_number: string
    address: string
    email: string
    password: string
}

const UserSchema: Schema<IUser> = new Schema({
    pass_id: {
        type: String,
        ref: 'Pass'
    },
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    phone_number: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
})

export const User = model('User', UserSchema)
