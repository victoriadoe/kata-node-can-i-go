import { Router, Request, Response } from 'express'
import places from './place/route/places'
import users from './user/route/users'
import pass from './pass/route/pass'
import auth from './auth/route/auth'

const router: Router = Router()

router.use('/places', places)
router.use('/users', users)
router.use('/pass', pass)
router.use('/auth', auth)

router.get('/', (req: Request, res: Response) => {
    return res.status(200).json({ message: 'Can I Go API' })
})

export default router
