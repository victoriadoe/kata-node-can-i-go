export type JwtTokenDto = {
    access_token: string
}
