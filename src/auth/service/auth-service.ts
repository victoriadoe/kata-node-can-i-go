import { LoginDto } from '../dto/login-dto'
import { JwtTokenDto } from '../dto/jwt-token-dto'
import UserService from '../../user/service/user-service'
import { AuthenticationFailureError, NotFoundError } from '../../middleware/exception/Exception'
import jwt from 'jsonwebtoken'
import { SECRET } from '../../config/lib/dotenv'

export default class AuthService {
    private userService: UserService = new UserService()

    logIn = async (loginDto: LoginDto): Promise<JwtTokenDto> => {
        const user = await this.userService.findByEmail(loginDto.email)
        if (!user) {
            throw new NotFoundError(`User with email ${loginDto.email} does not exist`)
        }
        const verifyPassword = await this.userService.verifyPassword(loginDto.password, user.password)
        if (!verifyPassword) {
            throw new AuthenticationFailureError('Passwords do not match')
        }
        const payload = {
            email: user.email,
            _id: user._id
        }
        return {
            access_token: this.generateToken(payload)
        }
    }

    generateToken = (payload: { email: string; _id: string | undefined }) => {
        return jwt.sign({ user: payload }, SECRET, { expiresIn: '2h' })
    }
}
