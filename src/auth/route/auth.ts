import { Router } from 'express'
import { autoCatch } from '../../middleware/auto-catch'
import AuthController from '../controller/auth-controller'
import { validateRequest } from '../../helpers/validators/request-validator'

const authController: AuthController = new AuthController()
const router: Router = Router()

router.post('/logIn', validateRequest, autoCatch(authController.logIn))

export default router
