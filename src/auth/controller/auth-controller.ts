import { NextFunction, Request, Response } from 'express'
import AuthService from '../service/auth-service'
import { BadRequestError } from '../../middleware/exception/Exception'
import { validateLogin } from '../../helpers/validators/payload-validator'

export default class AuthController {
    private authService: AuthService = new AuthService()

    logIn = async (req: Request, res: Response, next: NextFunction) => {
        const loginDto = validateLogin(req.body)
        if (loginDto.error) {
            return next(new BadRequestError(loginDto.error.details[0].message))
        }
        const token = await this.authService.logIn(loginDto.value)
        res.status(200).json({ token: token, message: 'User successfully logged in' })
    }
}
