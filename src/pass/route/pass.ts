import { Router } from 'express'
import { validateRequest } from '../../helpers/validators/request-validator'
import { autoCatch } from '../../middleware/auto-catch'
import PassController from '../controller/pass-controller'
import { requireAuthentication } from '../../middleware/auth'

const passController: PassController = new PassController()
const router: Router = Router()

router.get('/', requireAuthentication, autoCatch(passController.findAllPasses))
router.get('/:id', requireAuthentication, autoCatch(passController.findPassById))
router.post('/', validateRequest, requireAuthentication, autoCatch(passController.createPass))
router.put('/:id', validateRequest, requireAuthentication, autoCatch(passController.updatePass))
router.delete('/:id', requireAuthentication, autoCatch(passController.deletePass))

export default router
