import { NextFunction, Request, Response } from 'express'
import { validatePass } from '../../helpers/validators/payload-validator'
import PassService from '../service/pass-service'
import { BadRequestError, NotFoundError, UnauthorizedError } from '../../middleware/exception/Exception'
import { getDecodedPayload } from '../../middleware/auth'
import UserService from '../../user/service/user-service'

export default class PassController {
    private passService: PassService = new PassService()
    private userService: UserService = new UserService()

    findAllPasses = async (req: Request, res: Response) => {
        const allPasses = await this.passService.findAll()
        res.status(200).json({ passes: allPasses, message: 'Passes successfully retrieved' })
    }

    findPassById = async (req: Request, res: Response, next: NextFunction) => {
        await this.identityCheck(req, res, next)
        const pass = await this.passService.findById(req.params.id)
        if (!pass) {
            throw next(new NotFoundError(`Pass with id ${req.params.id} does not exist`))
        }
        res.status(200).json({ pass: pass, message: 'Pass successfully retrieved' })
    }

    createPass = async (req: Request, res: Response, next: NextFunction) => {
        const passDto = validatePass(req.body)
        if (passDto.error) {
            return next(new BadRequestError(passDto.error.details[0].message))
        }
        const pass = await this.passService.create(passDto.value)
        res.status(201).json({ pass: pass, message: 'Pass successfully created' })
    }

    updatePass = async (req: Request, res: Response, next: NextFunction) => {
        await this.identityCheck(req, res, next)
        const passDto = validatePass(req.body)
        if (passDto.error) {
            return next(new BadRequestError(passDto.error.details[0].message))
        }
        const updatedPass = {
            ...passDto.value,
            updated_at: new Date()
        }
        const pass = await this.passService.update(req.params.id, updatedPass)
        if (!pass) {
            throw next(new NotFoundError(`Pass with id ${req.params.id} does not exist`))
        }
        res.status(200).json({ pass: pass, message: 'Pass successfully updated' })
    }

    deletePass = async (req: Request, res: Response, next: NextFunction) => {
        const deletedPass = await this.passService.delete(req.params.id)
        if (!deletedPass) {
            throw next(new NotFoundError(`Pass with id ${req.params.id} does not exist`))
        }
        res.status(200).json({ message: 'Pass successfully deleted' })
    }

    identityCheck = async (req: Request, res: Response, next: NextFunction) => {
        const decodedToken = getDecodedPayload(req, res, next)
        if (decodedToken) {
            const connectedUser = await this.userService.findById(decodedToken.user._id)
            if (connectedUser && String(connectedUser.pass_id) !== req.params.id) {
                throw next(new UnauthorizedError("You cannot access someone else's pass"))
            }
        }
    }
}
