import { IPass } from '../schema/Pass'

export type CreateOrUpdatePassDto = Partial<IPass>
