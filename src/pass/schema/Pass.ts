import { Schema, model } from 'mongoose'

export interface IPass {
    _id?: string
    level: number
    created_at: Date
    updated_at: Date
}

const PassSchema = new Schema<IPass>({
    level: {
        type: Number,
        required: true,
        min: 1,
        max: 5
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
})

export const Pass = model('Pass', PassSchema)
