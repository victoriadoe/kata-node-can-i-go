import { CreateOrUpdatePassDto } from '../dto/create-or-update-pass-dto'
import { IPass, Pass } from '../schema/Pass'

export default class PassService {
    findAll = async (): Promise<IPass[]> => {
        return Pass.find()
    }

    findById = async (id: string): Promise<IPass | null> => {
        return Pass.findById(id)
    }

    create = async (createPass: CreateOrUpdatePassDto): Promise<CreateOrUpdatePassDto & IPass> => {
        return Pass.create(createPass)
    }

    update = async (id: string, updatePass: CreateOrUpdatePassDto): Promise<CreateOrUpdatePassDto | null> => {
        return Pass.findByIdAndUpdate(id, updatePass, { new: true })
    }

    delete = async (id: string) => {
        return Pass.findByIdAndDelete(id)
    }
}
