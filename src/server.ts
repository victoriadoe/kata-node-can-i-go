import app from './app'
import { SERVER_PORT } from './config/lib/dotenv'
import { mongoDBConnection } from './config/database/mongo-connection'

//Launch server
const PORT: number = parseInt(SERVER_PORT, 10)

app.listen(PORT, () => {
    mongoDBConnection().catch((e) => console.error(e))
    console.log(`Listening on port ${PORT}`)
})
