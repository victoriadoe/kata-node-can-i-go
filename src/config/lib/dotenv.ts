import * as dotenv from 'dotenv'
import path from 'path'

export const NODE_ENV = (process.env.NODE_ENV || 'dev') as 'test' | 'dev' | 'prod'
if (NODE_ENV === 'test') {
    dotenv.config({ path: path.resolve(process.cwd(), `.env.${NODE_ENV}`) })
}
dotenv.config()

const getOrThrow = (name: string) => {
    const val = process.env[name]
    if (typeof val === 'undefined') throw new Error(`Missing mandatory environment variable ${name}`)
    return val
}

export const SERVER_PORT = getOrThrow('SERVER_PORT')
export const MONGO_DB_URL = getOrThrow('MONGO_DB_URL')
export const SECRET = getOrThrow('SECRET')
