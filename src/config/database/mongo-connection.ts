import { connect } from 'mongoose'
import { MONGO_DB_URL } from '../lib/dotenv'
import { DatabaseError } from '../../middleware/exception/Exception'

export const mongoDBConnection = async () => {
    try {
        await connect(MONGO_DB_URL)
        console.log('Database successfully connected')
    } catch (e) {
        console.error(e)
        throw new DatabaseError()
    }
}
