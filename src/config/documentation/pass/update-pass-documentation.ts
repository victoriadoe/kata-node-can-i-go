import { BadRequestError, InternalServerError, NotFoundError } from '../error/error-documentation'
import { passResponse } from '../response/pass-response'

export const updatePass = {
    tags: ['Passes'],
    description: 'Update a pass',
    operationId: 'updatePass',
    parameters: [
        {
            name: 'id',
            in: 'path',
            description: 'pass uuid',
            required: true,
            type: 'string'
        }
    ],
    requestBody: {
        content: {
            'application/json': {
                schema: {
                    $ref: '#/components/schemas/updatePassBody'
                }
            }
        },
        required: true
    },
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: passResponse
                    }
                }
            }
        },
        '400': BadRequestError('Missing or invalid level'),
        '404': NotFoundError("Pass doesn't exist"),
        '500': InternalServerError
    }
}

export const updatePassBody = {
    type: 'object',
    properties: {
        level: {
            type: 'number',
            example: 4,
            required: false
        }
    }
}
