import { BadRequestError } from '../error/error-documentation'
import { passResponse } from '../response/pass-response'

export const createPass = {
    tags: ['Passes'],
    description: 'Create a new pass',
    operationId: 'createPass',
    requestBody: {
        content: {
            'application/json': {
                schema: {
                    $ref: '#/components/schemas/createPassBody'
                }
            }
        },
        required: true
    },
    responses: {
        '201': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: passResponse
                    }
                }
            }
        },
        '400': BadRequestError('Missing or invalid level')
    }
}

export const createPassBody = {
    type: 'object',
    properties: {
        level: {
            type: 'number',
            example: 3
        }
    }
}
