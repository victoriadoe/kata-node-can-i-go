import { passResponse } from '../response/pass-response'

export const getPass = {
    tags: ['Passes'],
    description: 'Retrieve a pass by id',
    operationId: 'retrieve pass',
    parameters: [
        {
            name: 'id',
            in: 'path',
            description: 'pass uuid',
            required: true,
            type: 'string'
        }
    ],
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: passResponse
                    }
                }
            }
        }
    }
}

export const getPasses = {
    tags: ['Passes'],
    description: 'Retrieve all passes',
    operationId: 'retrieve passes',
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'array',
                        items: {
                            type: 'object',
                            properties: passResponse
                        }
                    }
                }
            }
        }
    }
}
