import { InternalServerError, NotFoundError } from '../error/error-documentation'

export const deletePass = {
    tags: ['Passes'],
    description: 'Delete a pass',
    operationId: 'deletePass',
    parameters: [
        {
            name: 'id',
            in: 'path',
            description: 'pass uuid',
            required: true,
            type: 'string'
        }
    ],
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            message: {
                                type: 'string',
                                example: 'Pass deleted successfully!'
                            }
                        }
                    }
                }
            }
        },
        '404': NotFoundError("Pass doesn't exist"),
        '500': InternalServerError
    }
}
