import { InternalServerError, NotFoundError } from '../error/error-documentation'

export const deleteUser = {
    tags: ['Users'],
    description: 'Delete a user',
    operationId: 'deleteUser',
    parameters: [
        {
            name: 'id',
            in: 'path',
            description: 'user uuid',
            required: true,
            type: 'string'
        }
    ],
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            message: {
                                type: 'string',
                                example: 'User deleted successfully!'
                            }
                        }
                    }
                }
            }
        },
        '404': NotFoundError("User doesn't exist"),
        '500': InternalServerError
    }
}
