import { BadRequestError } from '../error/error-documentation'
import { userResponse } from '../response/user-response'

export const createUser = {
    tags: ['Users'],
    description: 'Create a new user',
    operationId: 'createUser',
    requestBody: {
        content: {
            'application/json': {
                schema: {
                    $ref: '#/components/schemas/createUserBody'
                }
            }
        },
        required: true
    },
    responses: {
        '201': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: userResponse
                    }
                }
            }
        },
        '400': BadRequestError('Missing or invalid properties')
    }
}

export const createUserBody = {
    type: 'object',
    properties: {
        pass_id: {
            type: 'string',
            example: '60564fcb544047cdc3844818'
        },
        first_name: {
            type: 'string',
            example: 'John Doe'
        },
        last_name: {
            type: 'string',
            example: 'john.doe@email.com'
        },
        age: {
            type: 'number',
            example: 40
        },
        phone_number: {
            type: 'string',
            example: '060102030405'
        },
        address: {
            type: 'string',
            example: '10 Rue Vauvilliers'
        }
    }
}
