import { BadRequestError, InternalServerError, NotFoundError } from '../error/error-documentation'
import { userResponse } from '../response/user-response'

export const updateUser = {
    tags: ['Users'],
    description: 'Update a user',
    operationId: 'updateUser',
    parameters: [
        {
            name: 'id',
            in: 'path',
            description: 'user uuid',
            required: true,
            type: 'string'
        }
    ],
    requestBody: {
        content: {
            'application/json': {
                schema: {
                    $ref: '#/components/schemas/updateUserBody'
                }
            }
        },
        required: true
    },
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: userResponse
                    }
                }
            }
        },
        '400': BadRequestError('Missing or invalid properties'),
        '404': NotFoundError("User doesn't exist"),
        '500': InternalServerError
    }
}

export const updateUserBody = {
    type: 'object',
    properties: {
        pass_id: {
            type: 'string',
            example: '60564fcb544047cdc3844818',
            required: false
        },
        first_name: {
            type: 'string',
            example: 'John Doe',
            required: false
        },
        last_name: {
            type: 'string',
            example: 'john.doe@email.com',
            required: false
        },
        age: {
            type: 'number',
            example: 40,
            required: false
        },
        phone_number: {
            type: 'string',
            example: '060102030405',
            required: false
        },
        address: {
            type: 'string',
            example: '10 Rue Vauvilliers',
            required: false
        }
    }
}
