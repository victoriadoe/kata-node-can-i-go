import { userResponse } from '../response/user-response'
import { NotFoundError, UnauthorizedError } from '../error/error-documentation'
import { placeResponse } from '../response/place-response'

export const getUser = {
    tags: ['Users'],
    description: 'Retrieve a user by id',
    operationId: 'retrieve user',
    parameters: [
        {
            name: 'id',
            in: 'path',
            description: 'user uuid',
            required: true,
            type: 'string'
        }
    ],
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: userResponse
                    }
                }
            }
        }
    }
}

export const getUsers = {
    tags: ['Users'],
    description: 'Retrieve all users',
    operationId: 'retrieve users',
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'array',
                        items: {
                            type: 'object',
                            properties: userResponse
                        }
                    }
                }
            }
        }
    }
}

export const hasAccess = {
    tags: ['Users'],
    description: 'Determine whether user has access to place',
    operationId: 'hasAccess',
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            message: {
                                type: 'string',
                                example: 'You can access this place'
                            }
                        }
                    }
                }
            }
        },
        '403': UnauthorizedError("Your pass level or age isn't sufficient to access this place"),
        '404': NotFoundError("Place doesn't exist")
    }
}

export const getAccessiblePlaces = {
    tags: ['Users'],
    description: 'Retrieve the list of places a user has access to',
    operationId: 'getAccessiblePlaces',
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'array',
                        items: {
                            type: 'object',
                            properties: placeResponse
                        }
                    }
                }
            }
        },
        '403': UnauthorizedError("You don't have access to any place"),
        '404': NotFoundError("User doesn't exist")
    }
}
