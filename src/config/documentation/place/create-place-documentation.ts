import { BadRequestError } from '../error/error-documentation'
import { placeResponse } from '../response/place-response'

export const createPlace = {
    tags: ['Places'],
    description: 'Create a new place',
    operationId: 'createPlace',
    requestBody: {
        content: {
            'application/json': {
                schema: {
                    $ref: '#/components/schemas/createPlaceBody'
                }
            }
        },
        required: true
    },
    responses: {
        '201': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: placeResponse
                    }
                }
            }
        },
        '400': BadRequestError('Missing or invalid properties')
    }
}

export const createPlaceBody = {
    type: 'object',
    properties: {
        phone_number: {
            type: 'string',
            example: '060102030405'
        },
        address: {
            type: 'string',
            example: '10 Rue Vauvilliers'
        },
        required_pass_level: {
            type: 'number',
            example: 3
        },
        required_age_level: {
            type: 'number',
            example: 30
        }
    }
}
