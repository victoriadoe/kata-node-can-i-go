import { placeResponse } from '../response/place-response'

export const getPlace = {
    tags: ['Places'],
    description: 'Retrieve a place by id',
    operationId: 'retrieve place',
    parameters: [
        {
            name: 'id',
            in: 'path',
            description: 'place uuid',
            required: true,
            type: 'string'
        }
    ],
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: placeResponse
                    }
                }
            }
        }
    }
}

export const getPlaces = {
    tags: ['Places'],
    description: 'Retrieve all places',
    operationId: 'retrieve places',
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'array',
                        items: {
                            type: 'object',
                            properties: placeResponse
                        }
                    }
                }
            }
        }
    }
}
