import { BadRequestError, InternalServerError, NotFoundError } from '../error/error-documentation'
import { placeResponse } from '../response/place-response'

export const updatePlace = {
    tags: ['Places'],
    description: 'Update a place',
    operationId: 'updatePlace',
    parameters: [
        {
            name: 'id',
            in: 'path',
            description: 'place uuid',
            required: true,
            type: 'string'
        }
    ],
    requestBody: {
        content: {
            'application/json': {
                schema: {
                    $ref: '#/components/schemas/updatePlaceBody'
                }
            }
        },
        required: true
    },
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: placeResponse
                    }
                }
            }
        },
        '400': BadRequestError('Missing or invalid properties'),
        '404': NotFoundError("Place doesn't exist"),
        '500': InternalServerError
    }
}

export const updatePlaceBody = {
    type: 'object',
    properties: {
        phone_number: {
            type: 'string',
            example: '060102030405',
            required: false
        },
        address: {
            type: 'string',
            example: '10 Rue Vauvilliers',
            required: false
        },
        required_pass_level: {
            type: 'number',
            example: 3,
            required: false
        },
        required_age_level: {
            type: 'number',
            example: 30,
            required: false
        }
    }
}
