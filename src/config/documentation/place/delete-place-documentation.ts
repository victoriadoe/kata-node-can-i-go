import { InternalServerError, NotFoundError } from '../error/error-documentation'

export const deletePlace = {
    tags: ['Places'],
    description: 'Delete a place',
    operationId: 'deletePlace',
    parameters: [
        {
            name: 'id',
            in: 'path',
            description: 'place uuid',
            required: true,
            type: 'string'
        }
    ],
    responses: {
        '200': {
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            message: {
                                type: 'string',
                                example: 'Place deleted successfully!'
                            }
                        }
                    }
                }
            }
        },
        '404': NotFoundError("Place doesn't exist"),
        '500': InternalServerError
    }
}
