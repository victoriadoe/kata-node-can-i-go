export const placeResponse = {
    _id: {
        type: 'string',
        example: '60564fcb544047cdc3844818'
    },
    address: {
        type: 'string',
        example: '10 Rue Vauvilliers'
    },
    phone_number: {
        type: 'string',
        required: true
    },
    required_pass_level: {
        type: 'number',
        example: 3
    },
    required_age_level: {
        type: 'number',
        example: 20
    }
}
