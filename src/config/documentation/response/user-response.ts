export const userResponse = {
    _id: {
        type: 'string',
        example: '60564fcb544047cdc3844818'
    },
    pass_id: {
        type: 'string',
        example: '60564fcb544047cdc3844818'
    },
    first_name: {
        type: 'string',
        example: 'John Doe'
    },
    last_name: {
        type: 'string',
        example: 'john.doe@email.com'
    },
    age: {
        type: 'number',
        example: 40
    },
    phone_number: {
        type: 'string',
        example: '060102030405'
    },
    address: {
        type: 'string',
        example: '10 Rue Vauvilliers'
    }
}
