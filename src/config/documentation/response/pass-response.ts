export const passResponse = {
    _id: {
        type: 'string',
        example: '60564fcb544047cdc3844818'
    },
    level: {
        type: 'number',
        example: 3
    },
    created_at: {
        type: 'string',
        example: '2024-01-15T13:18:58.475Z'
    },
    updated_at: {
        type: 'string',
        default: '2024-01-15T13:18:58.475Z'
    }
}
