export const BadRequestError = (message: string) => {
    return {
        description: 'Bad Request Error',
        content: {
            'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                        message: {
                            type: 'string',
                            example: message
                        }
                    }
                }
            }
        }
    }
}

export const InternalServerError = {
    description: 'Something Wrong Happened',
    content: {
        'application/json': {
            schema: {
                type: 'object',
                properties: {
                    message: {
                        type: 'string',
                        example: 'Something Wrong Happened'
                    }
                }
            }
        }
    }
}

export const NotFoundError = (message: string) => {
    return {
        description: 'Resource does not exist',
        content: {
            'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                        message: {
                            type: 'string',
                            example: message
                        }
                    }
                }
            }
        }
    }
}

export const UnauthorizedError = (message: string) => {
    return {
        description: 'You do not have sufficient privileges to access this resource',
        content: {
            'application/json': {
                schema: {
                    type: 'object',
                    properties: {
                        message: {
                            type: 'string',
                            example: message
                        }
                    }
                }
            }
        }
    }
}
