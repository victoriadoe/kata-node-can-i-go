import { createUser, createUserBody } from './user/create-user-documentation'
import { deleteUser } from './user/delete-user-documentation'
import { getAccessiblePlaces, getUser, getUsers, hasAccess } from './user/get-users-documentation'
import { updateUser, updateUserBody } from './user/update-user-documentation'
import { getPass, getPasses } from './pass/get-passes-documentation'
import { createPass, createPassBody } from './pass/create-pass-documentation'
import { updatePass, updatePassBody } from './pass/update-pass-documentation'
import { deletePass } from './pass/delete-pass-documentation'
import { getPlace, getPlaces } from './place/get-places-documentation'
import { createPlace, createPlaceBody } from './place/create-place-documentation'
import { updatePlace, updatePlaceBody } from './place/update-places-documentation'
import { deletePlace } from './place/delete-place-documentation'

export const swaggerDocumentation = {
    openapi: '3.0.1',
    info: {
        version: '1.0.0',
        title: 'Can I Go API',
        description: 'An application that manages Passes that deliver access to Users to private zones'
    },
    servers: [
        {
            url: 'http://localhost:3800/',
            description: 'Local Server'
        }
    ],
    tags: [
        {
            name: 'Passes'
        },
        {
            name: 'Users'
        },
        {
            name: 'Places'
        }
    ],
    paths: {
        '/users': {
            get: getUsers,
            post: createUser
        },
        '/users/{id}': {
            get: getUser,
            patch: updateUser,
            delete: deleteUser
        },
        '/users/{user_id}/{place_id}': {
            get: hasAccess
        },
        '/users/{user_id}/places': {
            post: getAccessiblePlaces
        },
        '/pass': {
            get: getPasses,
            post: createPass
        },
        '/pass/{id}': {
            get: getPass,
            put: updatePass,
            delete: deletePass
        },
        '/places': {
            get: getPlaces,
            post: createPlace
        },
        '/places/{id}': {
            get: getPlace,
            patch: updatePlace,
            delete: deletePlace
        }
    },
    components: {
        schemas: {
            createUserBody,
            updateUserBody,
            createPassBody,
            updatePassBody,
            createPlaceBody,
            updatePlaceBody
        }
    }
}
