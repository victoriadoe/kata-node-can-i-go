import request from 'supertest'
import app from '../src/app'

describe('Routes (E2E)', () => {
    describe('/', () => {
        it('should return message', async () => {
            const res = await request(app).get('/')
            expect(res.status).toEqual(200)
            expect(res.body.message).toBe('Can I Go API')
        })
    })
})
