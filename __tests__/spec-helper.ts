import { connection } from 'mongoose'

export const clearDatabase = async () => {
    const collections = connection.collections
    await Promise.all(
        Object.values(collections).map(async (collection) => {
            await collection.deleteMany({})
        })
    )
}


