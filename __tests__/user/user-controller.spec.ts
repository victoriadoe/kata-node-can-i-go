import request from 'supertest'
import app from '../../src/app'
import { clearDatabase } from '../spec-helper'
import mongoose from 'mongoose'
import { IUser } from '../../src/user/schema/User'
import UserService from '../../src/user/service/user-service'
import { getFakeUser } from './fake/user-fake'
import { CreateUserDto } from '../../src/user/dto/create-user-dto'
import { mongoDBConnection } from '../../src/config/database/mongo-connection'

describe('User Controller (E2E)', () => {
    let user: IUser

    let userService: UserService

    beforeAll(async () => {
        await mongoDBConnection()

        userService = new UserService()
    })

    beforeEach(async () => {
        await clearDatabase()
        user = await userService.create(getFakeUser())
    })

    describe('/GET user', () => {
        it('should return all users', async () => {
            const res = await request(app).get('/users')
            expect(res.status).toEqual(200)
            expect(res.body.message).toBe('Users successfully retrieved')
        })
    })

    describe('GET user/id', () => {
        it('should return user of corresponding id', async () => {
            const res = await request(app).get('/users/' + user._id)
            expect(res.status).toEqual(200)
            expect(res.body.message).toBe('User successfully retrieved')
        })
    })

    describe('/POST user', () => {
        it('should create user', async () => {
            const new_user: CreateUserDto = {
                age: 25,
                email: 'test@test.com',
                first_name: 'firstname',
                last_name: 'lastname',
                pass_id: '65a8f8372febf8ca34d94e65',
                password: 'hash_password',
                address: 'address',
                phone_number: '123456'
            }
            const res = await request(app).post('/users').send(new_user)
            expect(res.status).toEqual(201)
        })
    })

    afterAll(() => {
        mongoose.connection.close()
    })
})
