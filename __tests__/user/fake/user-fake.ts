import { CreateUserDto } from '../../../src/user/dto/create-user-dto'
import { faker } from '@faker-js/faker'

export const getFakeUser = (): CreateUserDto => {
    return {
        address: faker.location.streetAddress(),
        phone_number: faker.phone.number(),
        age: faker.number.int({ max: 90 }),
        email: faker.internet.email(),
        first_name: faker.person.firstName(),
        last_name: faker.person.lastName(),
        pass_id: faker.database.mongodbObjectId(),
        password: faker.internet.password()
    }
}
