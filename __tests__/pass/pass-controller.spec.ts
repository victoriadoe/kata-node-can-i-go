import request from 'supertest'
import app from '../../src/app'
import { clearDatabase } from '../spec-helper'
import PassService from '../../src/pass/service/pass-service'
import { getFakePass } from './fake/pass-fake'
import { IPass } from '../../src/pass/schema/Pass'
import mongoose from 'mongoose'
import { mongoDBConnection } from '../../src/config/database/mongo-connection'

describe('Pass Controller (E2E)', () => {
    let pass: IPass

    let passService: PassService

    let token: string | undefined

    beforeAll(async () => {
        await mongoDBConnection()

        passService = new PassService()

        token = process.env.TOKEN
    })

    beforeEach(async () => {
        await clearDatabase()
        pass = await passService.create(getFakePass())
    })

    describe('/GET pass', () => {
        it('should return all passes', async () => {
            const res = await request(app).get('/pass').set('Authorization', `Bearer ${token}`)
            expect(res.status).toEqual(200)
            expect(res.body.message).toBe('Passes successfully retrieved')
        })
    })

    describe('GET pass/id', () => {
        it('should return pass of corresponding id', async () => {
            const res = await request(app).get('/pass/' + pass._id).set('Authorization', `Bearer ${token}`)
            expect(res.status).toEqual(200)
            expect(res.body.message).toBe('Pass successfully retrieved')
        })
    })

    describe('/POST pass', () => {
        it('should create pass', async () => {
            const newPass = { level: 5 }
            const res = await request(app).post('/pass').send(newPass).set('Authorization', `Bearer ${token}`)
            expect(res.status).toEqual(201)
            expect(res.body.message).toBe('Pass successfully created')
        })
    })

    describe('/PUT pass/id', () => {
        it('should update pass', async () => {
            const res = await request(app)
                .put('/pass/' + pass._id)
                .set('Authorization', `Bearer ${token}`)
                .send({ level: 3 })
            expect(res.status).toEqual(200)
            expect(res.body.message).toBe('Pass successfully updated')
        })
    })

    afterAll(() => {
        mongoose.connection.close()
    })
})
