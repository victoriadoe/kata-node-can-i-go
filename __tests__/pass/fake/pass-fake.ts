import { faker } from '@faker-js/faker'
import { CreateOrUpdatePassDto } from '../../../src/pass/dto/create-or-update-pass-dto'

export const getFakePass = (): CreateOrUpdatePassDto => {
    return {
        level: faker.number.int({ min: 1, max: 5 })
    }
}
