import { faker } from '@faker-js/faker'
import { CreateOrUpdatePlaceDto } from '../../../src/place/dto/create-or-update-place-dto'

export const getFakePlace = (): CreateOrUpdatePlaceDto => {
    return {
        address: faker.location.streetAddress(),
        phone_number: faker.phone.number(),
        required_age_level: faker.number.int({ max: 90 }),
        required_pass_level: faker.number.int({ min: 1, max: 5 })
    }
}
