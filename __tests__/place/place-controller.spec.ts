import request from 'supertest'
import app from '../../src/app'
import { clearDatabase } from '../spec-helper'
import mongoose from 'mongoose'
import { IPlace } from '../../src/place/schema/Place'
import PlaceService from '../../src/place/service/place-service'
import { getFakePlace } from './fake/place-fake'
import { CreateOrUpdatePlaceDto } from '../../src/place/dto/create-or-update-place-dto'
import { mongoDBConnection } from '../../src/config/database/mongo-connection'

describe('Place Controller (E2E)', () => {
    let place: IPlace

    let placeService: PlaceService

    let token: string | undefined

    beforeAll(async () => {
        await mongoDBConnection()

        placeService = new PlaceService()

        token = process.env.TOKEN
    })

    beforeEach(async () => {
        await clearDatabase()
        place = await placeService.create(getFakePlace())
    })

    describe('/GET place', () => {
        it('should return all places', async () => {
            const res = await request(app).get('/places')
            expect(res.status).toEqual(200)
            expect(res.body.message).toBe('Places successfully retrieved')
        })
    })

    describe('GET place/id', () => {
        it('should return place of corresponding id', async () => {
            const res = await request(app).get('/places/' + place._id)
            expect(res.status).toEqual(200)
            expect(res.body.message).toBe('Place successfully retrieved')
        })
    })

    describe('/POST place', () => {
        it('should create place', async () => {
            const new_place: CreateOrUpdatePlaceDto = {
                address: 'address',
                phone_number: '123456',
                required_age_level: 19,
                required_pass_level: 4
            }
            const res = await request(app).post('/places').set('Authorization', `Bearer ${token}`).send(new_place)
            expect(res.status).toEqual(201)
            expect(res.body.message).toBe('Place successfully created')
        })
    })

    afterAll(() => {
        mongoose.connection.close()
    })
})
