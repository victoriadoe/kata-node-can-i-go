import request from 'supertest'
import app from '../src/app'
import { getFakeUser } from './user/fake/user-fake'
import { mongoDBConnection } from '../src/config/database/mongo-connection'
import mongoose from 'mongoose'
import { CreateUserDto } from '../src/user/dto/create-user-dto'

export default async () => {
    await mongoDBConnection()

    await retrieveToken(getFakeUser())

    await mongoose.connection.close()
}

const retrieveToken = async (user: CreateUserDto) => {
    await request(app).post('/users').send(user)
    const res = await request(app).post('/auth/logIn').send({ email: user.email, password: user.password })
    process.env.TOKEN = res.body.token.access_token
}
